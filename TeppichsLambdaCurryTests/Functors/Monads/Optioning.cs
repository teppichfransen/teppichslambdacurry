using TeppichsLambdaCurry;
using static TeppichsLambdaCurry.Optioning;
using static TeppichsLambdaCurry.Helpers;

namespace TeppichsLambdaCurryTests.Functors.Monads;

public sealed class Optioning
{
    [Fact]
    private void ParsesZeroStringToZeroInt() => Assert.Equal(0, ParseToInt("0"));

    [Fact]
    private void ParsesNonNumberToNoneType()
    {
        Option<int> parsed = ParseToInt("a");

        Assert.Equal(0, parsed.AsEnumerable().Count());
    }
}