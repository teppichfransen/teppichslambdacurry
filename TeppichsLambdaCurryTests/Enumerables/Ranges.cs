using static TeppichsLambdaCurry.Enumerables.Ranges;

namespace TeppichsLambdaCurryTests.Enumerable
{
    public sealed class Ranges
    {
        [Fact]
        private void IntRangeFromZeroToOne()
        {
            List<int> list       = 0.To(1).ToList();
            
            Assert.Equal(0, list[0]);
            Assert.Equal(1, list[1]);
        }
    }
}