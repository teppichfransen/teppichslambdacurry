﻿using System.Collections.Generic;

namespace TeppichsLambdaCurry.Enumerables
{
    public static class Ranges
    {
        public static IEnumerable<int> To(this int from, int to)
        {
            return Range(from, to);
        }

        public static IEnumerable<int> Range(int from, int to)
        {
            for (int i = from; i <= to; i++)
                yield return i;
        }

        public static IEnumerable<int> Range(int from, int to, int step)
        {
            for (int i = from; i <= to; i += step)
                yield return i;
        }

        public static IEnumerable<uint> Range(uint from, uint to, uint step)
        {
            for (uint i = from; i <= to; i += step)
                yield return i;
        }

        public static IEnumerable<char> Range(char from, char to)
        {
            for (char i = from; i <= to; i++)
                yield return i;
        }

        public static IEnumerable<float> Range(float from, float to, float step)
        {
            for (float i = from; i <= to; i += step)
                yield return i;
        }
    }
}