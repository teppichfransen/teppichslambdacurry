﻿using System;

namespace TeppichsLambdaCurry
{
    public static class ApplyingSideEffects
    {
        public static Func<T, T> Tap<T>(Action<T> action) => t =>
        {
            action(t);

            return t;
        };

        public static TR Pipe<T, TR>(this T t, Func<T, TR> func)   => func(t);
        public static T  Pipe<T>(this     T t, Action<T>   action) => Tap(action)(t);
    }
}