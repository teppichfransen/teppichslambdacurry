﻿using System;

namespace TeppichsLambdaCurry
{
    public static class Currying
    {
        #region Curries

        public static Func<T1, Func<T2, TR>> Curry<T1, T2, TR>(this Func<T1, T2, TR> func) => t1 => t2 => func(t1, t2);

        public static Func<T1, Func<T2, Func<T3, TR>>> Curry<T1, T2, T3, TR>(this Func<T1, T2, T3, TR> func) =>
            t1 => t2 => t3 => func(t1, t2, t3);

        public static Func<T1, Func<T2, Func<T3, Func<T4, TR>>>>
            Curry<T1, T2, T3, T4, TR>(this Func<T1, T2, T3, T4, TR> func) =>
            t1 => t2 => t3 => t4 => func(t1, t2, t3, t4);

        #endregion

        #region Partial Curries

        public static Func<T1, Func<T2, T3, TR>> CurryFirst<T1, T2, T3, TR>(this Func<T1, T2, T3, TR> func) =>
            t1 => (t2, t3) => func(t1, t2, t3);

        public static Func<T1, Func<T2, T3, T4, TR>>
            CurryFirst<T1, T2, T3, T4, TR>(this Func<T1, T2, T3, T4, TR> func) =>
            t1 => (t2, t3, t4) => func(t1, t2, t3, t4);

        public static Func<T2, Func<T1, T3, TR>> CurrySecond<T1, T2, T3, TR>(this Func<T1, T2, T3, TR> func) =>
            t2 => (t1, t3) => func(t1, t2, t3);

        public static Func<T2, Func<T1, T3, T4, TR>>
            CurrySecond<T1, T2, T3, T4, TR>(this Func<T1, T2, T3, T4, TR> func) =>
            t2 => (t1, t3, t4) => func(t1, t2, t3, t4);

        public static Func<T3, Func<T1, T2, TR>> CurryThird<T1, T2, T3, TR>(this Func<T1, T2, T3, TR> func) =>
            t3 => (t1, t2) => func(t1, t2, t3);

        public static Func<T3, Func<T1, T2, T4, TR>>
            CurryThird<T1, T2, T3, T4, TR>(this Func<T1, T2, T3, T4, TR> func) =>
            t3 => (t1, t2, t4) => func(t1, t2, t3, t4);

        public static Func<T4, Func<T1, T2, T3, TR>>
            CurryFourth<T1, T2, T3, T4, TR>(this Func<T1, T2, T3, T4, TR> func) =>
            t4 => (t1, t2, t3) => func(t1, t2, t3, t4);

        #endregion
    }
}