﻿using System;
using System.Collections.Generic;

namespace TeppichsLambdaCurry
{
    using static Helpers;

    public struct Option<T>
    {
        private readonly T?   value;
        private readonly bool isSome;

        internal Option(T value)
        {
            this.value = value ?? throw new ArgumentException();
            isSome     = true;
        }

        public static implicit operator Option<T>(NoneType _)     => default;
        public static implicit operator Option<T>(T        value) => value is null ? None : new Option<T>(value);

        public TR Match<TR>(Func<TR> noneFunc, Func<T, TR> someFunc) => isSome ? someFunc(value!) : noneFunc();

        public IEnumerable<T> AsEnumerable()
        {
            if (isSome)
                yield return value!;
        }

        public override string ToString() => isSome ? $"Some({value}" : "None";

        public Option<TReturn> Map<TReturn>(Func<T, TReturn> func) => Match(() => None, (t) => Some(func(t)));
    }
}