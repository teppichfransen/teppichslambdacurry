﻿using System;
using System.Collections.Generic;
using static TeppichsLambdaCurry.Helpers;
using Unit = System.ValueTuple;

namespace TeppichsLambdaCurry
{
    public static class OptionExtensions
    {
        public static Option<TReturn> Map<T2, TReturn>(this Option<T2> optT, Func<T2, TReturn> func) =>
            optT.Match(() => None, (t) => Some(func(t)));

        public static Option<Unit> Foreach<T>(this Option<T> option, Action<T> action) => Map(option, action.ToFunc());

        public static Option<TReturn> Bind<T, TReturn>(this Option<T> option, Func<T, Option<TReturn>> func) =>
            option.Match(() => None, (t) => func(t));

        public static Option<T> Where<T>(this Option<T> option, Func<T, bool> predicate) =>
            option.Match(() => None, (t) => predicate(t) ? option : None);

        public static IEnumerable<TReturn>
            Bind<T, TReturn>(this Option<T> option, Func<T, IEnumerable<TReturn>> func) =>
            option.AsEnumerable().Bind(func);
    }
}