﻿using System;
using System.Collections.Generic;
using System.Linq;
using Unit = System.ValueTuple;

namespace TeppichsLambdaCurry
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<TReturn> Map<T, TReturn>(this IEnumerable<T> ts, Func<T, TReturn> func) =>
            ts.Select(func);

        public static IEnumerable<Unit> ForEach<T>(this IEnumerable<T> ts, Action<T> action) =>
            ts.Map(action.ToFunc())
              .ToList(); //TODO: make ToImmutableList, for that get the Nuget package System.Collections.Immutable

        public static IEnumerable<TR> Bind<T, TR>(this IEnumerable<T> ts, Func<T, IEnumerable<TR>> func)
        {
            foreach (T t in ts)
            foreach (TR tr in func(t))
                yield return tr;
        }

        public static IEnumerable<T> List<T>(params T[] items) =>
            items.ToList(); //TODO: make ToImmutableList, for that get the Nuget package System.Collections.Immutable

        public static IEnumerable<TReturn> Bind<T, TReturn>(this IEnumerable<T>      enumerable,
                                                            Func<T, Option<TReturn>> func) =>
            enumerable.Bind(t => func(t).AsEnumerable());
    }
}