﻿using System;
using System.Collections.Generic;
using static TeppichsLambdaCurry.Helpers;

namespace TeppichsLambdaCurry
{
    public static class Optioning
    {
        public static Option<TValue> Lookup<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            dictionary.TryGetValue(key, out TValue value);

            return value;
        }

        #region Parsing

        public static Option<int> ParseToInt(string s)
        {
            int.TryParse(s, out int result);

            return result;
        }

        public static Option<uint> ParseToUInt(string s)
        {
            uint.TryParse(s, out uint result);

            return result;
        }

        public static Option<float> ParseToFloat(string s)
        {
            float.TryParse(s, out float result);

            return result;
        }

        public static Option<double> ParseToDouble(string s)
        {
            double.TryParse(s, out double result);

            return result;
        }

        public static Option<DateTime> ParseToDateTime(string s) =>
            DateTime.TryParse(s, out DateTime d) ? Some(d) : None;

        #endregion
    }
}