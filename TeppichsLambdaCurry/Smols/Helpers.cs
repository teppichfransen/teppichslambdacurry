﻿using Unit = System.ValueTuple;

namespace TeppichsLambdaCurry
{
    public static class Helpers
    {
        public static NoneType  None             => default;
        public static Option<T> Some<T>(T value) => new(value);

        public static Unit Unit() => default;
    }
}