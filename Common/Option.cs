﻿namespace TeppichsLambdaCurry;

public record Option<T>
{
    public static implicit operator Option<T>(NoneType _) => new None<T>();
}